using Assets.Scripts.Character;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] Movement movement;

        void Start() => movement.Initialize();

        void Update()
        {
            var xAxis = Input.GetAxisRaw("Horizontal");
            movement.OnMoveSideways(xAxis);

            if(Input.GetButtonDown("Jump"))
                movement.OnJump();

            if (Input.GetKeyDown(KeyCode.J))
                movement.ApplyKnockback();
        }
    }
}
