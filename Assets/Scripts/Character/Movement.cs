using UnityEngine;

namespace Assets.Scripts.Character
{
    public class Movement : MonoBehaviour
    {
        [SerializeField] Transform mainTransform;
        [SerializeField] Gravity gravityComponent;
        [SerializeField] EnvironmentDetection detector;
        [SerializeField] SideMovement sideMovement;
        [SerializeField] Jump jump;
        [SerializeField] Knockback knockback;
        [SerializeField] float maxSideSpeed;

        [SerializeField] CharacterView view;


        float yVelocity;
        float xVelocity;

        float xVelScaled => xVelocity * Time.deltaTime;
        float yVelScaled => yVelocity * Time.deltaTime;

        public void Initialize()
        {
            gravityComponent.Initialize(ModifyYVelocity);
            sideMovement.Initialize(ModifyXVelocity, SetXVelocity);
            jump.Initialize(ModifyYVelocity);
            knockback.Initialize(SetXVelocity, SetYVelocity);
        }

        void FixedUpdate()
        {
            gravityComponent.ApplyGravity();
            CheckForGrounding();
            CheckForCeiling();
            CheckForWalls();

            mainTransform.Translate(new Vector3(xVelScaled, yVelScaled));

            sideMovement.ApplyBreak(xVelocity);
            view.SetSideSpeed(xVelocity);
            view.SetVerticalSpeed(yVelocity);
        }

        public void ApplyKnockback() => 
            knockback.Do(1);


        void CheckForWalls()
        {
            if (detector.HasSideWall(transform.position, xVelScaled))
                SetXVelocity(0);
        }

        void CheckForGrounding()
        {
            if (yVelocity <= 0 && detector.IsGrounded(mainTransform.position, yVelScaled))
            {
                SetYVelocity(0);
                var pos = transform.position;
                pos = detector.StepFromGround(transform.position, 1);
                transform.position = pos;
            }
        }

        void CheckForCeiling()
        {
            if (yVelocity > 0 && detector.IsCeilingHit(mainTransform.position, yVelScaled))
                SetYVelocity(0);
        }

        void ModifyXVelocity(float delta) => 
            xVelocity = Mathf.Clamp(xVelocity + delta, -maxSideSpeed, maxSideSpeed);

        void SetXVelocity(float value) =>
            xVelocity = value;
        void SetYVelocity(float value) =>
            yVelocity = value;

        void ModifyYVelocity(float delta) => yVelocity += delta;

        public void OnMoveSideways(float xAxis) => sideMovement.Move(xAxis);

        public void OnJump()
        {
            if (detector.IsGrounded(mainTransform.position, 0.2f))
            {
                jump.NormalJump();
                view.OnJump();
            }
        }
    }
}
