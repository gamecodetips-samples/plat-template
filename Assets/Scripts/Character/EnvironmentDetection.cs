﻿using UnityEngine;

namespace Assets.Scripts.Character
{
    public class EnvironmentDetection : MonoBehaviour
    {
        [SerializeField] LayerMask obstacleLayer;
        [SerializeField] float actorHeight;
        [SerializeField] float actorWidth;
        [SerializeField] bool isPlanetoidal;

        public Vector2 StepFromGround(Vector2 origin, float lookAhead)
        {
            var hit = Physics2D.Raycast(origin, -transform.up, 10, obstacleLayer);
            return isPlanetoidal
                ? hit.point + (hit.normal * actorHeight / 2)
                : hit.point + (Vector2.up * actorHeight / 2);
        }

        public Vector2 NormalFromGround(Vector2 origin, float lookAhead)
        {
            var hit = Physics2D.Raycast(origin, -transform.up, 10, obstacleLayer);
            return hit.normal;
        }
        
        public bool IsGrounded(Vector2 origin, float lookAhead)
        {
            Debug.DrawRay(origin, -transform.up * (actorHeight / 2 + Mathf.Abs(lookAhead)), Color.blue);
            return Physics2D.Raycast(origin, -transform.up, actorHeight / 2 + Mathf.Abs(lookAhead), obstacleLayer);
        }

        public bool IsCeilingHit(Vector2 origin, float lookAhead)
        {
            Debug.DrawRay(origin, transform.up * (actorHeight / 2 + Mathf.Abs(lookAhead)), Color.blue);
            return Physics2D.Raycast(origin, transform.up, actorHeight / 2 + Mathf.Abs(lookAhead), obstacleLayer);
        }

        public bool HasSideWall(Vector2 origin, float lookAhead)
        {
            Debug.DrawRay(origin, transform.right * (actorWidth / 2 + Mathf.Abs(lookAhead)), Color.red);
            Debug.DrawRay(origin, -transform.right * (actorWidth / 2 + Mathf.Abs(lookAhead)), Color.red);

            return lookAhead > 0
                ? Physics2D.Raycast(origin, transform.right, actorWidth / 2 + Mathf.Abs(lookAhead), obstacleLayer)
                : Physics2D.Raycast(origin, -transform.right, actorWidth / 2 + Mathf.Abs(lookAhead), obstacleLayer);
        }
    }
}

